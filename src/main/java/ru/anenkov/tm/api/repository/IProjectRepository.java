package ru.anenkov.tm.api.repository;

import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.User;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project findOneById(String userId, String id);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneByName(String userId, String name);

    Project removeOneById(String userId, String id);

    void merge(Collection<Project> projects);

    Project merge(Project project);

    void merge(Project... projects);

    void load(Collection<Project> projects);

    void load(Project... projects);

    void clear();

    void load(Project project);

    List<Project> getListProjects();

}
