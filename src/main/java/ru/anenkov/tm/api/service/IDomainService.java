package ru.anenkov.tm.api.service;

import ru.anenkov.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);

}
