package ru.anenkov.tm.service;

import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.api.service.ICommandService;
import ru.anenkov.tm.command.AbstractCommand;

import java.util.List;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

}
