package ru.anenkov.tm.command.user;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;

public class UserProfileViewCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Profile";
    }

    @Override
    public String description() {
        return "Show user profile";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW USER PROFILE]");
        final User user = serviceLocator.getAuthService().showUserProfile();
        System.out.println(user);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
