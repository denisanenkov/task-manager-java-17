package ru.anenkov.tm.command.data.base64;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public class DataBase64ClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Data-base64-clear";
    }

    @Override
    public String description() {
        return "Clear base64 data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        final File file = new File(DataConst.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}

