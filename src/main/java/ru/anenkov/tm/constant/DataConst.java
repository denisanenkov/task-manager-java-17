package ru.anenkov.tm.constant;

public interface DataConst {

    String FILE_BINARY = "./data.bin";

    String FILE_BASE64 = "./database64.base64";

}
